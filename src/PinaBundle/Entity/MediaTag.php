<?php

namespace PinaBundle\Entity;

/**
 * MediaTag
 */
class MediaTag
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \PinaBundle\Entity\Medias
     */
    private $media;

    /**
     * @var \PinaBundle\Entity\Tags
     */
    private $tag;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set media
     *
     * @param \PinaBundle\Entity\Medias $media
     *
     * @return MediaTag
     */
    public function setMedia(\PinaBundle\Entity\Medias $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \PinaBundle\Entity\Medias
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set tag
     *
     * @param \PinaBundle\Entity\Tags $tag
     *
     * @return MediaTag
     */
    public function setTag(\PinaBundle\Entity\Tags $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \PinaBundle\Entity\Tags
     */
    public function getTag()
    {
        return $this->tag;
    }
}

