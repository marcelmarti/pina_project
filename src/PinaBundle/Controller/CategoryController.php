<?php

namespace PinaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use PinaBundle\Entity\Category;
use PinaBundle\Form\CategoryType;


class CategoryController extends Controller
{
	private $session;
	
	public function __construct() {
		$this->session=new Session();
	}

	public function indexAction(){
		$em = $this->getDoctrine()->getEntityManager();
		$tag_repo=$em->getRepository("PinaBundle:Tag");
		$tags=$tag_repo->findAll();
		
		return $this->render("PinaBundle:Tag:index.html.twig",array(
			"tags" => $tags
		));
	}
	
	public function addAction(Request $request){
		$category = new Category();
		$form = $this->createForm(CategoryType::class,$category);
		
		$form->handleRequest($request);
		
		if($form->isSubmitted()){
			if($form->isValid()){
				
				$em = $this->getDoctrine()->getEntityManager();
				
				$category = new Category();
				$category->setName($form->get("name")->getData());
				$category->setDescription($form->get("description")->getData());
				
				$em->persist($category);
				$flush = $em->flush();
				
				if($flush==null){
					$status = "La etiqueta se ha creado correctamente !!";
				}else{
					$status ="Error al añadir la etiqueta!!";
				}
				
			}else{
				$status = "La etiqueta no se ha creado, porque el formulario no es valido !!";
			}
			
			$this->session->getFlashBag()->add("status", $status);
			return $this->redirectToRoute("pina_index_category");
		}
		
		
		return $this->render("PinaBundle:Category:add.html.twig",array(
			"form" => $form->createView()
		));
	}
	
	public function deleteAction($id){
		$em = $this->getDoctrine()->getEntityManager();
		$category_repo=$em->getRepository("PinaBundle:Tag");
		$category=$category_repo->find($id);
		
		if(count($category->getEntryCategory())==0){
			$em->remove($category);
			$em->flush();
		}
		
		return $this->redirectToRoute("blog_index_tag");
	}
}