CREATE DATABASE IF NOT EXISTS pinaprojectdb;
USE pinaprojectdb;

CREATE TABLE users(
id          int(255) auto_increment not null,
role        varchar(20),
name        varchar(255),
surname     varchar(255),
email       varchar(255),
password    varchar(255),
image       varchar(255),
CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE categories(
id          int(255) auto_increment not null,
name        varchar(255),
description text,
CONSTRAINT pk_categories PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE medias(
id          int(255) auto_increment not null,
user_id     int(255) not null,
category_id int(255) not null,
url         text,
title       varchar(255),
status      varchar(20),
CONSTRAINT pk_medias PRIMARY KEY(id),
CONSTRAINT fk_medias_users foreign key(user_id) references users(id),
CONSTRAINT fk_medias_categories foreign key(category_id) references categories(id)
)ENGINE=InnoDb;

CREATE TABLE tags(
id          int(255) auto_increment not null,
name        varchar(255),
description text,
CONSTRAINT pk_tags PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE media_tag(
id          int(255) auto_increment not null,
media_id    int(255) not null,
tag_id      int(255) not null,
CONSTRAINT pk_media_tag PRIMARY KEY(id),
CONSTRAINT fk_media_tag_medias foreign key(media_id) references medias(id),
CONSTRAINT fk_media_tag_tags foreign key(tag_id) references tags(id)
)ENGINE=InnoDb;